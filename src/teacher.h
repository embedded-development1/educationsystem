#pragma once
#include <iostream>
#include <person.h>
using namespace std;

class Teacher : public Person{
    int id;
    string campus;
    string program;
    string course;
    int salary;
    public:
    Teacher(int id, string firstName, string lastName, int age, string campus, string program, string course, int salary);
    int GetID();
    string GetCourse();
    string GetCampus();
    string GetProgram();
    int GetSalary();
    void ChangeCourse(string& name);
    template<class B>
    void serialize(B& buf) const;
    template<class B>
    void parse(B& buf);
};