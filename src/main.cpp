#include <iostream>
#include <limits>
#include <algorithm>
#include <vector>
#include <student.h>
#include <teacher.h>
#include <campus.h>
#include <program.h>
#include <course.h>
#include <person.h>
#include <filemanager.h>
using namespace std;

static const char* const FILENAME_STUDENT = "student.txt";
static const char* const FILENAME_TEACHER = "teacher.txt";
static const char* const FILENAME_CAMPUS = "campus.txt";
static const char* const FILENAME_PROGRAM = "program.txt";
static const char* const FILENAME_COURSE = "course.txt";

enum school{
    campuses,
    programs,
    courses,
    teachers,
    students
};

enum State{
    start,
    quit,
    read,
    write,
    addStudent,
    addTeacher,
    addCampus,
    addProgram,
    addCourse,
    findbyid,
    removeFromCourse,
    addToCourse
};
vector<Student> studentList;
vector<Teacher> teacherList;
vector<Campus> campusList;
vector<Program> programList;
vector<Course> courseList;

State status = start;
bool running = true;
bool unsavedChanges = false;

//See if campus exists in files
string GetCampus(){
    string temp;
    bool campusExists = false;
    cout << "Enter campus name (none for no campus): " << endl;
    cin >> temp;
    for (size_t i = 0; i < campusList.size(); i++){
        if(temp == campusList[i].GetName() || temp == "none"){
            campusExists = true;
        }
    }
    while(!campusExists){
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "Please type existing campus name or none!"<<endl;
        cin >> temp;
        for (size_t i = 0; i < campusList.size(); i++){
            if(temp == campusList[i].GetName() || temp == "none"){
                campusExists = true;
            }
        }
    }
    return temp;
}

//See if program exists in files
string GetProgram(){
    string temp;
    bool programExists = false;
    cout << "Enter program name (none for no program): " << endl;
    cin >> temp;
    for (size_t i = 0; i < programList.size(); i++){
        if(temp == programList[i].GetName() || temp == "none"){
            programExists = true;
        }
    }
    while(!programExists){
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "Please type existing program name or none!"<<endl;
        cin >> temp;
        for (size_t i = 0; i < programList.size(); i++){
            if(temp == programList[i].GetName() || temp == "none"){
                programExists = true;
            }
        }
    }
    return temp;
}

//See if course exists in files
string GetCourse(){
    string temp;
    bool courseExists = false;
    cout << "Enter course name (none for no course): " << endl;
    cin >> temp;
    for (size_t i = 0; i < courseList.size(); i++){
        if(temp == courseList[i].GetName() || temp == "none"){
            courseExists = true;
        }
    }
    while(!courseExists){
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "Please type existing course name or none!"<<endl;
        cin >> temp;
        for (size_t i = 0; i < courseList.size(); i++){
            if(temp == courseList[i].GetName() || temp == "none"){
                courseExists = true;
            }
        }
    }
    return temp;
}

//Adds student to the list
void AddStudent(){
    string firstname;
    string lastname;
    int age;
    string campus;
    string program;
    string course;
    cout << "Enter first name: " << endl;
    cin >> firstname;
    cout << "Enter last name: " << endl;
    cin >> lastname;
    cout << "Enter age: " << endl;
    cin >> age;
    campus = GetCampus();
    program = GetProgram();
    course = GetCourse();
    int id = studentList.size() + 1;
    Student newStudent(id, firstname, lastname, age, campus, program, course);
    studentList.emplace_back(newStudent);
    cout << "Student added" << endl;
    unsavedChanges = true;
}

//Adds teacher to the list
void AddTeacher(){
    string firstname;
    string lastname;
    int age;
    string campus;
    string program;
    string course;
    int salary;
    cout << "Enter first name: " << endl;
    cin >> firstname;
    cout << "Enter last name: " << endl;
    cin >> lastname;
    cout << "Enter age: " << endl;
    cin >> age;
    campus = GetCampus();
    program = GetProgram();
    course = GetCourse();
    cout << "Enter salary: " << endl;
    cin >> salary;
    int id = teacherList.size() + 1;
    Teacher newTeacher(id, firstname, lastname, age, campus, program, course, salary);
    teacherList.emplace_back(newTeacher);
    cout << "Teacher added" << endl;
    unsavedChanges = true;
}

//Adds campus to the list
void AddCampus(){
    string name;
    cout << "Enter name: " << endl;
    cin >> name;
    int id = campusList.size() + 1;
    Campus newCampus(id, name);
    campusList.emplace_back(newCampus);
    cout << "Campus added" << endl;
    unsavedChanges = true;
}

//Adds program to the list
void AddProgram(){
    string name;
    string dateBegin;
    string dateEnd;
    string campus;
    cout << "Enter name: " << endl;
    cin >> name;
    cout << "Enter start date: " << endl;
    cin >> dateBegin;
    cout << "Enter end date: " << endl;
    cin >> dateEnd;
    campus = GetCampus();
    int id = programList.size() + 1;
    Program newProgram(id, name, dateBegin, dateEnd, campus);
    programList.emplace_back(newProgram);
    cout << "Program added" << endl;
    unsavedChanges = true;
}

//Adds course to the list
void AddCourse(){
    string name;
    string dateBegin;
    string dateEnd;
    string campus;
    string program;
    cout << "Enter name: " << endl;
    cin >> name;
    cout << "Enter start date: " << endl;
    cin >> dateBegin;
    cout << "Enter end date: " << endl;
    cin >> dateEnd;
    campus = GetCampus();
    program = GetProgram();
    int id = courseList.size() + 1;
    Course newCourse(id, name, dateBegin, dateEnd, campus, program);
    courseList.emplace_back(newCourse);
    cout << "Course added" << endl;
    unsavedChanges = true;
}
//Finds object of different categories by id
void FindByID(school type, int id){
    if(type == students){
        if(!(id > studentList.size()) && !(id < 0)){
            id--;
            cout << studentList[id].GetID() << ' ' << studentList[id].GetFirstName() << ' ' << studentList[id].GetLastName() << ' ' << studentList[id].GetAge() << ' ' << studentList[id].GetCampus() << ' ' << studentList[id].GetProgram() << ' ' << studentList[id].GetCourse() << ' ' << endl;
        }
        else{
            cout << "No students with ID: " << id << endl;
        }
    }
    else if(type == teachers){
        if(!(id > teacherList.size()) && !(id < 0)){
            id--;
            cout << teacherList[id].GetID() << ' ' << teacherList[id].GetFirstName() << ' ' << teacherList[id].GetLastName() << ' ' << teacherList[id].GetAge() << ' ' << teacherList[id].GetCampus() << ' ' << teacherList[id].GetProgram() << ' ' << teacherList[id].GetCourse() << ' ' << teacherList[id].GetSalary() << endl;
        }
        else{
            cout << "No teachers with ID: " << id << endl;
        }
    }
    else if(type == campuses){
        if(!(id > campusList.size()) && !(id < 0)){
            id--;
            cout << campusList[id].GetID() << ' ' << campusList[id].GetName() << endl;
        }
        else{
            cout << "No campus with ID: " << id << endl;
        }
    }
    else if(type == programs){
        if(!(id > programList.size()) && !(id < 0)){
            id--;
            cout << programList[id].GetID() << ' ' << programList[id].GetName() << ' ' << programList[id].GetDateStart() << ' ' << programList[id].GetDateEnd() << ' ' << programList[id].GetCampus() << endl;
        }
        else{
            cout << "No programs with ID: " << id << endl;
        }
    }
    else if(type == courses){
        if(!(id > courseList.size()) && !(id < 0)){
            id--;
            cout << courseList[id].GetID() << ' ' << courseList[id].GetName() << ' ' << courseList[id].GetDateStart() << ' ' << courseList[id].GetDateEnd() << ' ' << courseList[id].GetCampus() << ' ' << courseList[id].GetProgram() << endl;
        }
        else{
            cout << "No courses with ID: " << id << endl;
        }
    }
}

//Removes course from student or teacher
void RemoveFromCourse(school type, int id){
    string newText = "NONE";
    if(type == students){
        if(!(id > studentList.size()) && !(id < 0)){
            id--;
            studentList[id].ChangeCourse(newText);
            cout << "Student has been removed from course" << endl;
            unsavedChanges = true;
        }
        else{
            cout << "No students with ID: " << id << endl;
        }
    }
    else if(type == teachers){
        if(!(id > teacherList.size()) && !(id < 0)){
            id--;
            teacherList[id].ChangeCourse(newText);
            cout << "Teacher has been removed from course" << endl;
            unsavedChanges = true;
        }
        else{
            cout << "No teachers with ID: " << id << endl;
        }
    }
    else{
        cout << "Format not supported here!" << id << endl;        
    }
}
//Adds course to student or teacher
void AddToCourse(school type, int id, string course){
    bool courseExists = false;
    for (size_t i = 0; i < courseList.size(); i++){
        if(course == courseList[i].GetName()){
            courseExists = true;
        }
    }
    if(courseExists){
        if(type == students){
            if(!(id > studentList.size()) && !(id < 0)){
                id--;
                studentList[id].ChangeCourse(course);
                cout << "Student has been added to course" << endl;
                unsavedChanges = true;
            }
            else{
                cout << "No students with ID: " << id << endl;
            }
        }
            else if(type == teachers){
                if(!(id > teacherList.size()) && !(id < 0)){
                    id--;
                    teacherList[id].ChangeCourse(course);
                    cout << "Teacher has been added to course" << endl;
                    unsavedChanges = true;
                }
                else{
                    cout << "No teachers with ID: " << id << endl;
                }
            }
        else{
            cout << "Format not supported here!" << endl;        
        }
    }
    else{
        cout << "Course: " << course << " does not exist!" << endl;   
    }
}
//Writes each list to their files
void Write(){
    MakeStudentFile(studentList, FILENAME_STUDENT);
    MakeTeacherFile(teacherList, FILENAME_TEACHER);
    MakeCampusFile(campusList, FILENAME_CAMPUS);
    MakeProgramFile(programList, FILENAME_PROGRAM);
    MakeCourseFile(courseList, FILENAME_COURSE);
    cout << "Files done written" << endl;
    unsavedChanges = false;
}

//Reads each list from each file and prints to console
void Read(){
    studentList = ReadStudentFile(FILENAME_STUDENT);
    teacherList = ReadTeacherFile(FILENAME_TEACHER);
    campusList = ReadCampusFile(FILENAME_CAMPUS);
    programList = ReadProgramFile(FILENAME_PROGRAM);
    courseList = ReadCourseFile(FILENAME_COURSE);
    cout << "Students: " << endl;
    for (size_t i = 0; i < studentList.size(); i++){
        cout << studentList[i].GetID() << ' ' << studentList[i].GetFirstName() << ' ' << studentList[i].GetLastName() << ' ' << studentList[i].GetAge() << ' ' << studentList[i].GetCampus() << ' ' << studentList[i].GetProgram() << ' ' <<  studentList[i].GetCourse() << endl;
    }
    cout << "Teachers: " << endl;
    for (size_t i = 0; i < teacherList.size(); i++){
        cout << teacherList[i].GetID() << ' ' << teacherList[i].GetFirstName() << ' ' << teacherList[i].GetLastName() << ' ' << teacherList[i].GetAge() << ' ' << teacherList[i].GetCampus() << ' ' << teacherList[i].GetProgram() << ' ' << teacherList[i].GetCourse() << ' ' << teacherList[i].GetSalary() << endl;
    }
    cout << "Campuses: " << endl;
    for (size_t i = 0; i < campusList.size(); i++){
        cout << campusList[i].GetID() << ' ' << campusList[i].GetName() << endl;
    }
    cout << "Programs: " << endl;
    for (size_t i = 0; i < programList.size(); i++){
        cout << programList[i].GetID() << ' ' << programList[i].GetName() << ' ' << programList[i].GetDateStart() << ' ' << programList[i].GetDateEnd() << ' ' << programList[i].GetCampus() << endl;
    }
    cout << "Courses: " << endl;
    for (size_t i = 0; i < courseList.size(); i++){
        cout << courseList[i].GetID() << ' ' << courseList[i].GetName() << ' ' << courseList[i].GetDateStart() << ' ' << courseList[i].GetDateEnd() << ' ' << courseList[i].GetCampus() << ' ' << courseList[i].GetProgram() << endl;
    }
}

//Start function to see what status the program is at the moment and what needs to be changed
void Start(){
    string x;
    cout << "Welcome to education portal!" << endl;
    cout << "Type commands for following: " << endl;
    cout << "r = read files" << endl;
    cout << "w = write files" << endl;
    cout << "as = add student" << endl;
    cout << "at = add teacher" << endl;
    cout << "ac = add campus" << endl;
    cout << "ap = add program" << endl;
    cout << "ak = add course" << endl;
    cout << "f = find by ID" << endl;
    cout << "rfc = remove student from course" << endl;
    cout << "atc = add student to course" << endl;
    cout << "q = quit" << endl;
    if(unsavedChanges){
        cout << "You have unsaved changes! Press w to write to files" << endl;
    }
    cin >> x;
    if(x == "r"){
        status = read;
    }
    else if(x == "q"){
        status = quit;
    }
    else if(x == "w"){
        status = write;
    }
    else if(x == "as"){
        status = addStudent;
    }
    else if(x == "at"){
        status = addTeacher;
    }
    else if(x == "ac"){
        status = addCampus;
    }
    else if(x == "ap"){
        status = addProgram;
    }
    else if(x == "ak"){
        status = addCourse;
    }
    else if(x == "f"){
        status = findbyid;
    }
    else if(x == "rfc"){
        status = removeFromCourse;
    }
    else if(x == "atc"){
        status = addToCourse;
    }
    else{
        cout << "No such command! Try Again" << endl;
    }
}

//Main function, controlls what functions needs to run and if the program needs termination
int main(){
    studentList = ReadStudentFile(FILENAME_STUDENT);
    teacherList = ReadTeacherFile(FILENAME_TEACHER);
    campusList = ReadCampusFile(FILENAME_CAMPUS);
    programList = ReadProgramFile(FILENAME_PROGRAM);
    courseList = ReadCourseFile(FILENAME_COURSE);
    school sco = students;
    string schoolType;
    string coursename;
    int id;
    while(running){
        switch(status){
            case start:
                Start();
                break;
            case read:
                Read();
                status = start;
                break;
            case write:
                Write();
                status = start;
                break;
            case addStudent:
                AddStudent();
                status = start;
                break;
            case addTeacher:
                AddTeacher();
                status = start;
                break;
            case addCampus:
                AddCampus();
                status = start;
                break;
            case addProgram:
                AddProgram();
                status = start;
                break;
            case addCourse:
                AddCourse();
                status = start;
                break;
            case findbyid:
                cout << "Find what?(s,t,c,p,k)" << endl;
                cin >> schoolType;
                while(schoolType != "t" && schoolType != "s" && schoolType != "c" && schoolType != "p" && schoolType != "k"){
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout << "Please type s,t,c,p or k!"<<endl;
                    cin >> schoolType;
                }
                if(schoolType == "s"){
                    sco = students;
                }
                else if(schoolType == "t"){
                    sco = teachers;
                }   
                else if(schoolType == "c"){
                    sco = campuses;
                }  
                else if(schoolType == "p"){
                    sco = programs;
                }    
                else{
                    sco = courses;
                } 
                cout << "Type ID: " << endl;
                cin >> id;
                FindByID(sco, id);
                status = start;
                break;
            case removeFromCourse:
                cout << "Find what?(s,t)" << endl;
                cin >> schoolType;
                while(schoolType != "t" && schoolType != "s"){
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout << "Please type s or t!"<<endl;
                    cin >> schoolType;
                }
                if(schoolType == "s"){
                    sco = students;
                }
                else{
                    sco = teachers;
                } 
                cout << "Type ID: " << endl;
                cin >> id;
                RemoveFromCourse(sco, id);
                status = start;
                break;
            case addToCourse:
                cout << "Find what?(s,t)" << endl;
                cin >> schoolType;
                while(schoolType != "t" && schoolType != "s"){
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout << "Please type s,t or c!"<<endl;
                    cin >> schoolType;
                }
                if(schoolType == "s"){
                    sco = students;
                }
                else if(schoolType == "t"){
                    sco = teachers;
                }     
                else{
                    sco = courses;
                } 
                cout << "Type ID: " << endl;
                cin >> id;
                cout << "Type course name: " << endl;
                cin >> coursename;
                AddToCourse(sco, id, coursename);
                status = start;
                break;
            case quit:
                running = false;
                break;
        }
    }
    return 0;
}