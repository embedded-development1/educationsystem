#include <iostream>
#include <person.h>
using namespace std;

Person::Person(){
    firstName = "";
    lastName = "";
    age = 0;
}

Person::Person(string firstName, string lastName, int age){
    this->firstName = firstName;
    this->lastName = lastName;
    this->age = age;
}

string Person::GetFirstName(){
    return firstName;
}

string Person::GetLastName(){
    return lastName;
}

int Person::GetAge(){
    return age;
}