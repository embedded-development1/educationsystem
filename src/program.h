#pragma once
#include <iostream>
#include <campus.h>
using namespace std;

class Program : public Campus{
    protected:
        string dateBegin;
        string dateEnd;
        string campus;
    public:
        Program();
        Program(int ID, string name, string dateBegin, string dateEnd, string campus);
        string GetDateStart();
        string GetDateEnd();
        string GetCampus();
        template<class B>
        void serialize(B& buf) const;
        template<class B>
        void parse(B& buf);
};