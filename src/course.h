#pragma once
#include <iostream>
#include <program.h>
using namespace std;

class Course : public Program {
    protected:
        string program;
    public:
        Course(int ID, string name, string dateBegin, string dateEnd, string campus, string program);
        string GetProgram();
        template<class B>
        void serialize(B& buf) const;
        template<class B>
        void parse(B& buf);
};