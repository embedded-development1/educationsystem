#include <iostream>
#include <student.h>
using namespace std;

Student::Student(int id, string firstName, string lastName, int age, string campus, string program, string course)
:Person(firstName, lastName, age){
    this->id = id;
    this->campus = campus;
    this->program = program;
    this->course = course;
}

int Student::GetID(){
    return id;
}

string Student::GetCampus(){
    return campus;
}

string Student::GetProgram(){
    return program;
}

string Student::GetCourse(){
    return course;
}

void Student::ChangeCourse(string& name){
    course = name;
}

template<class B>
void Student::serialize(B& buf) const{
    buf << id << firstName << lastName << age << campus << program << course;
}

template<class B>
void Student::parse(B& buf){
    buf >> id >> firstName >> lastName >> age >> campus >> program >> course;
}