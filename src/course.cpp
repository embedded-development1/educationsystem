#include <iostream>
#include <program.h>
#include <course.h>
using namespace std;

Course::Course(int ID, string name_, string startDate, string endDate, string campus, string program):
Program(ID, name_, startDate, endDate, campus){
    this->program = program;
}

string Course::GetProgram(){
    return program;
}

template<class B>
void Course::serialize(B& buf) const{
    buf << id << name << dateBegin << dateEnd << campus << program;
}

template<class B>
void Course::parse(B& buf){
    buf >> id >> name >> dateBegin >> dateEnd >> campus >> program;
}