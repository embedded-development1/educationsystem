#include <iostream>
#include <course.h>
using namespace std;

Campus::Campus(){
    
}

Campus::Campus(int ID, string name){
    id = ID;
    this->name = name;
}

int Campus::GetID(){
    return id;
}

string Campus::GetName(){
    return name;
}

template<class B>
void Campus::serialize(B& buf) const{
    buf << id << name;
}

template<class B>
void Campus::parse(B& buf){
    buf >> id >> name;
}