#include <iostream>
#include <fstream>
#include <vector>
#include <person.h>
#include <student.h>
#include <teacher.h>
#include <campus.h>
#include <program.h>
#include <course.h>
#include <filesystem>
using namespace std;

//Adds data from a student vector to a file
void MakeStudentFile(vector<Student> studentList, string fileName){
    ofstream fout;
    try{
        fout.open(fileName);
        if(!fout){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }

    for (size_t i = 0; i < studentList.size(); i++)
    {
        fout << studentList[i].GetID() << ' ' << studentList[i].GetFirstName() << ' ' << studentList[i].GetLastName() << ' ' << studentList[i].GetAge() << ' ' << studentList[i].GetCampus() << ' ' << studentList[i].GetProgram() << ' ' << studentList[i].GetCourse() << '\n';
    }  
    fout.close();
}

//Adds data from a teacher vector to a file
void MakeTeacherFile(vector<Teacher> teacherList, string fileName){
    ofstream fout;
    try{
        fout.open(fileName);
        if(!fout){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }

    for (size_t i = 0; i < teacherList.size(); i++)
    {
        fout << teacherList[i].GetID() << ' ' << teacherList[i].GetFirstName() << ' ' << teacherList[i].GetLastName() << ' ' << teacherList[i].GetAge() << ' ' << teacherList[i].GetCampus() << ' ' << teacherList[i].GetProgram() << ' ' << teacherList[i].GetCourse() << ' ' << teacherList[i].GetSalary() << '\n';
    }  
    fout.close();
}

//Adds data from a campus vector to a file
void MakeCampusFile(vector<Campus> campusList, string fileName){
    ofstream fout;
    try{
        fout.open(fileName);
        if(!fout){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }

    for (size_t i = 0; i < campusList.size(); i++)
    {
        fout << campusList[i].GetID() << ' ' << campusList[i].GetName() << '\n';
    }  
    fout.close();
}

//Adds data from a program vector to a file
void MakeProgramFile(vector<Program> programList, string fileName){
    ofstream fout;
    try{
        fout.open(fileName);
        if(!fout){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }

    for (size_t i = 0; i < programList.size(); i++)
    {
        fout << programList[i].GetID() << ' ' << programList[i].GetName() << ' ' << programList[i].GetDateStart() << ' ' << programList[i].GetDateEnd() << ' ' << programList[i].GetCampus() << '\n';
    }  
    fout.close();
}

//Adds data from a course vector to a file
void MakeCourseFile(vector<Course> courseList, string fileName){
    ofstream fout;
    try{
        fout.open(fileName);
        if(!fout){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }

    for (size_t i = 0; i < courseList.size(); i++)
    {
        fout << courseList[i].GetID() << ' ' << courseList[i].GetName() << ' ' << courseList[i].GetDateStart() << ' ' << courseList[i].GetDateEnd() << ' ' << courseList[i].GetCampus() << ' ' << courseList[i].GetProgram() << '\n';
    }  
    fout.close();
}

//Makes a empty file with line as name
void MakeEmptyFile(string line){
    ofstream output(line);
}

//Reads data from a file and add it to a student vector
vector<Student> ReadStudentFile(string fileName){
    ifstream fin;
    if(!filesystem::exists(fileName)){
        MakeEmptyFile(fileName);
    }
    try{
        fin.open(fileName);
        if(!fin){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }
    vector<Student> students;
    int id;
    string fName;
    string lName;
    int age;
    string campus;
    string program;
    string course;
    while(fin >> id >> fName >> lName >> age >> campus >> program >> course){
        Student temp(id, fName, lName, age, campus, program, course);
        students.emplace_back(temp);
    }
    fin.close();
    return students;
}

//Reads data from a file and add it to a teacher vector
vector<Teacher> ReadTeacherFile(string fileName){
    ifstream fin;
    if(!filesystem::exists(fileName)){
        MakeEmptyFile(fileName);
    }
    try{
        fin.open(fileName);
        if(!fin){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }
    vector<Teacher> teachers;
    int id;
    string fName;
    string lName;
    int age;
    string campus;
    string program;
    string course;
    int salary;
    while(fin >> id >> fName >> lName >> age >> campus >> program >> course >> salary){
        Teacher temp(id, fName, lName, age, campus, program, course, salary);
        teachers.emplace_back(temp);
    }
    fin.close();
    return teachers;
}

//Reads data from a file and add it to a campus vector
vector<Campus> ReadCampusFile(string fileName){
    ifstream fin;
    if(!filesystem::exists(fileName)){
        MakeEmptyFile(fileName);
    }
    try{
        fin.open(fileName);
        if(!fin){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }
    vector<Campus> campuses;
    int id;
    string name;
    while(fin >> id >> name){
        Campus temp(id, name);
        campuses.emplace_back(temp);
    }
    fin.close();
    return campuses;
}

//Reads data from a file and add it to a program vector
vector<Program> ReadProgramFile(string fileName){
    ifstream fin;
    if(!filesystem::exists(fileName)){
        MakeEmptyFile(fileName);
    }
    try{
        fin.open(fileName);
        if(!fin){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }
    vector<Program> programs;
    int id;
    string name;
    string dateS;
    string dateE;
    string campus;;
    while(fin >> id >> name >> dateS >> dateE >> campus){
        Program temp(id, name, dateS, dateE, campus);
        programs.emplace_back(temp);
    }
    fin.close();
    return programs;
}

//Reads data from a file and add it to a course vector
vector<Course> ReadCourseFile(string fileName){
    ifstream fin;
    if(!filesystem::exists(fileName)){
        MakeEmptyFile(fileName);
    }
    try{
        fin.open(fileName);
        if(!fin){
            throw;
        }
    }
    catch(...){
        cerr<< "Error in opening this file"<<endl;
    }
    vector<Course> courses;
    int id;
    string name;
    string dateS;
    string dateE;
    string campus;
    string program;
    while(fin >> id >> name >> dateS >> dateE >> campus >> program){
        Course temp(id, name, dateS, dateE, campus, program);
        courses.emplace_back(temp);
    }
    fin.close();
    return courses;
}
