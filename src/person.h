#pragma once
#include <iostream>
using namespace std;

class Person{
    protected:
        string firstName;
        string lastName;
        int age;
    public:   
        Person();
        Person(string firstName, string lastName, int age);
        string GetFirstName();
        string GetLastName();
        int GetAge();
};