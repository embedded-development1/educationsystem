#pragma once
#include <iostream>
#include <person.h>
using namespace std;

class Student : public Person{
    int id;
    string campus;
    string program;
    string course;
    public:
    Student(int id, string firstName, string lastName, int age, string campus, string program, string course);
    int GetID();
    string GetCourse();
    string GetCampus();
    string GetProgram();
    void ChangeCourse(string& name);
    template<class B>
    void serialize(B& buf) const;
    template<class B>
    void parse(B& buf);
};