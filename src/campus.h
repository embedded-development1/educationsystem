#pragma once
#include <iostream>
using namespace std;

class Campus{
    protected:
        int id;
        string name;
    public:
        Campus();
        Campus(int ID, string name);
        string GetName();
        int GetID();
        template<class B>
        void serialize(B& buf) const;
        template<class B>
        void parse(B& buf);
};