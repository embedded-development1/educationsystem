#include <iostream>
#include <person.h>
#include <teacher.h>
using namespace std;

Teacher::Teacher(int id, string firstName, string lastName, int age, string campus, string program, string course, int salary):
Person(firstName, lastName, age){
    this->id = id;
    this->campus = campus;
    this->program = program;
    this->course = course;
    this->salary = salary;
}

int Teacher::GetID(){
    return id;
}

string Teacher::GetCampus(){
    return campus;
}

string Teacher::GetProgram(){
    return program;
}

string Teacher::GetCourse(){
    return course;
}

int Teacher::GetSalary(){
    return salary;
}

void Teacher::ChangeCourse(string& name){
    course = name;
}

template<class B>
void Teacher::serialize(B& buf) const{
    buf << id << firstName << lastName << age << campus << program << course << salary;
}

template<class B>
void Teacher::parse(B& buf){
    buf >> id >> firstName >> lastName >> age >> campus >> program >> course >> salary;
}