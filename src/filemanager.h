#pragma once
#include <iostream>
#include <vector>
#include <student.h>
#include <teacher.h>
#include <campus.h>
#include <program.h>
#include <course.h>
using namespace std;
vector<Student> ReadStudentFile(string fileName);
vector<Teacher> ReadTeacherFile(string fileName);
vector<Campus> ReadCampusFile(string fileName);
vector<Program> ReadProgramFile(string fileName);
vector<Course> ReadCourseFile(string fileName);
void MakeStudentFile(vector<Student> studentList, string fileName);
void MakeTeacherFile(vector<Teacher> teacherList, string fileName);
void MakeProgramFile(vector<Program> programList, string fileName);
void MakeCourseFile(vector<Course> courseList, string fileName);
void MakeCampusFile(vector<Campus> campusList, string fileName);
void MakeEmptyFile(string line);