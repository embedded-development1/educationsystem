#include <iostream>
#include <program.h>
#include <campus.h>
using namespace std;

Program::Program(){}

Program::Program(int ID, string name, string dateBegin, string dateEnd, string campus) :
Campus(ID, name){
    this->dateBegin = dateBegin;
    this->dateEnd = dateEnd;
    this->campus = campus;
}

string Program::GetDateStart(){
    return dateBegin;
}

string Program::GetDateEnd(){
    return dateEnd;
}

string Program::GetCampus(){
    return campus;
}

template<class B>
void Program::serialize(B& buf) const{
    buf << id << name << dateBegin << dateEnd << campus;
}

template<class B>
void Program::parse(B& buf)
{
    buf >> id >> name >> dateBegin >> dateEnd >> campus;
}