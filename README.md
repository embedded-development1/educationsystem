# Education System

## Background
The education system is a program that lets you store information about students, teacher, campuses, programs and courses in files and edit the information within the program.

## Description
This program was created with C++ to store information about different categories in education like teachers and courses. 

It works by making the user write down the information that each category needs and then the user can store this information to a file for each category. Users can also display this information on the console and modify data on certain categories.

The program also checks if certain words exists in other categories like in the campus, program and courses lists when adding data that requires this information.

## Installation
Clone it to .git and compile it with g++ to run it. 
```
- cd filelocation/src
- g++ main.cpp filemanager.cpp course.cpp program.cpp campus.cpp person.cpp student.cpp teacher.cpp -o main
- ./main
```
Program was created in C++ with VScode togheter with WSL(Windows subsystems for linux) and Cmake to compile the project.

Cmake is recommended for ease of compiling the project.

## Usage
A program used to store information about teachers, students, campuses, programs and courses.
The following commands can be used in this program:

- r (reads the files and prints them out on the console)

- w (writes the content from the vectors to the files)

- as (makes user write information about a student and adds it to the list. Campus, program and course must exist in the lists for the name to be valid, otherwise type "none")

- at (makes user write information about a teacher and adds it to the list. Campus, program and course must exist in the lists for the name to be valid, otherwise type "none")

- ac (makes user write information about a campus and adds it to the list.)

- ap (makes user write information about a program and adds it to the list. Campus must exist in the lists for the name to be valid, otherwise type "none")

- ak (makes user write information about a course and adds it to the list. Campus and program must exist in the lists for the name to be valid, otherwise type "none")

- f (finds and prints data based on the id that user wants from chosen list)

- rfc (removes student based on id from a course. Course name will be none)

- rtc (adds student based on id to a course. Course name must exist in the lists for it to work)

- q (quits the program)

Make sure to write to file when you have added or changed data, otherwise the information will be lost.

![StartImage](https://gitlab.com/embedded-development1/educationsystem/-/raw/main/images/Start.PNG)*Start*

![ReadImage](https://gitlab.com/embedded-development1/educationsystem/-/raw/main/images/read.PNG)*Read*

![AddImage](https://gitlab.com/embedded-development1/educationsystem/-/raw/main/images/studentExample.PNG)*Add student*

![RemoveImage](https://gitlab.com/embedded-development1/educationsystem/-/raw/main/images/removeCourse.PNG)*Remove course*


## Support
Send email to martin.dahren@se.experis.com for help with any problem you are having.

## Contributing
You are welcome to clone the project and make small changes to the program.
You can also fork the project and add new features if you give credit to the author. 
Bigger changes should have an issue opened to discuss what changes you want to do.

## License
[MIT](https://choosealicense.com/licenses/mit/)
